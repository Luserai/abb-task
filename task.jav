Calculate the factorial of the number n
import java.util.Scanner;
public class Main{
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int eded = input.nextInt();
        long factorial = 1;
        for (int i = 1; i <= eded; i++) {
            factorial *= i;
        }
        System.out.println(factorial);
    }
}



Calculate the sum of the digits of a given number
import java.util.Scanner;
public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Bir eded girin: ");
        int eded = input.nextInt();
        int sum = 0;
        int a = eded;
        while (a != 0) {
            int digit = a % 10;
            sum += digit;
            a /= 10;
        }
        System.out.println(sum);
    }
}


Find a number that is a mirror image of the sequence of digits of a given number, for example, given the number 123, output 321
import java.util.Scanner;
public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Bir eded girin: ");
        int eded = input.nextInt();
        int a = eded;
        int terseded=0;
        while (a != 0) {
            int digit = a % 10;
            terseded=terseded*10+digit;
            a /= 10;
        }
        System.out.println(terseded);
    }
}

Find the minimum element of an array
import java.util.Arrays;
public class Main {
    public static void main(String[] args) {
        int[] array = {5, 2, 8, 1, 3};
        Arrays.sort(array);
        int[] subArray = Arrays.copyOf(array,1);
        System.out.println(Arrays.toString(subArray));
    }
}


Find the index of the minimum element of the array
import java.util.Arrays;
public class Main {
    public static void main(String[] args) {
        int[] array = {5, 2, 8, 1, 3};
        Arrays.sort(array);
        int[] subArray = Arrays.copyOfRange(array, array.length-1, array.length);
        System.out.println(Arrays.toString(subArray));
    }
}

Calculate the sum of array elements with odd indexes
public class Main {
    public static void main(String[] args) {
        int[] array = {5, 2, 8, 1, 3};
        int sum = 0;
        for (int i : array) {
            if (i % 2 != 0) {
                sum += i;
            }
        }
        System.out.println(sum);
    }
}


Make an array reverse (an array in the opposite direction)
import java.util.Arrays;
public class Main {
    public static void main(String[] args) {
        int[] array = {5, 2, 8, 1, 3};
        Arrays.sort(array);
        System.out.println(Arrays.toString(array));
    }
}

Count the number of odd elements of the array
import java.util.ArrayList;
import java.util.List;
public class Main {
    public static void main(String[] args) {
        int[] array = {5, 2, 8, 1, 3};
        List<Integer> tekededler = new ArrayList<>();

        for (int i = 0; i < array.length; i++) {
            if (array[i] % 2 != 0) {
                tekededler.add(array[i]);
            }
        }
        System.out.println("Tek ededler: " + tekededler);
    }
}

Swap the first and second half of the array, for example, for the array 1 2 3 4, the result is 3 4 1 2.

public class Main {
    public static void main(String[] args) {
        int[] array = {1, 2, 3, 4};
        int length = array.length;
        int halfLength = length / 2;
        for (int i = 0; i < halfLength; i++) {
            int temp = array[i];
            array[i] = array[i + halfLength];
            array[i + halfLength] = temp;
        }
        for (int i = 0; i < length; i++) {
            System.out.print(array[i] + " ");
        }
    }
}
